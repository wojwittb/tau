import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "my_blog.settings")

import django
django.setup()

import random
from blog.models import Post
from django.contrib.auth.models import User
from django.utils import timezone
from faker import Faker

fakegen = Faker()

def populate(N=10):
    for entry in range(N):
        usr = User.objects.get_by_natural_key('test')

        fake_title = fakegen.company()
        fake_text = fakegen.paragraph()

        post = Post.objects.get_or_create(author = usr, title = fake_title, text = fake_text, published_date = timezone.now())

if __name__ == '__main__':
    print("Populating script!")
    populate(5)
    print("Populating complete!")
