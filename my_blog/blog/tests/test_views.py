from django.test import TestCase
from blog.models import Comment,Post
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


# Create your tests here.
class AboutViewTest(TestCase):
    def test_view_url_exists(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        response = self.client.get(reverse('about'))
        self.assertEqual(response.status_code, 200)

    def test_view_correct_template(self):
        response = self.client.get(reverse('about'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'about.html')

class PostListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_posts = 10

        User.objects.create_superuser('admin', 'admin@test.com', 'pass')

        for i in range(number_of_posts):
            post = Post.objects.create(author = User.objects.get(id=1), title = "Test Post %s" % i, text = "Test text %s" % i)
            post.publish()

    def test_view_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)

    def test_view_correct_template(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_list.html')

    def test_right_amount_of_posts(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)

        self.assertTrue(len(response.context['post_list'])==10)

class PostDetailViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")

    def test_view_url_exists(self):
        response = self.client.get('/post/1')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        response = self.client.get(reverse('post_detail', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

    def test_view_correct_template(self):
        response = self.client.get(reverse('post_detail', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_detail.html')

class CreatePostViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")

    def test_view_url_exists(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get('/post/new/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_new'))
        self.assertEqual(response.status_code, 200)

    def test_view_redirect_template(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'registration/login.html')

    def test_view_logged_in_template(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_new'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_form.html')

class PostUpdateViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")

    def test_view_url_exists(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get('/post/1/edit/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_edit', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

    def test_view_redirect_template(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'registration/login.html')

    def test_view_logged_in_template(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_edit', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_form.html')

class PostDeleteViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")

    def test_view_url_exists(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get('/post/1/remove/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_remove', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

    def test_view_redirect_template(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'registration/login.html')

    def test_view_logged_in_template(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_remove', kwargs={'pk':1}))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_confirm_delete.html')

class DraftListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_posts = 10

        User.objects.create_superuser('admin', 'admin@test.com', 'pass')

        for i in range(number_of_posts):
            post = Post.objects.create(author = User.objects.get(id=1), title = "Test Post %s" % i, text = "Test text %s" % i)

    def test_view_url_exists(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get('/drafts/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_reverse(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_draft_list'))
        self.assertEqual(response.status_code, 200)

    def test_view_logged_in_template(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_draft_list'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'blog/post_list.html')

    def test_view_redirect_template(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'registration/login.html')

    def test_right_amount_of_posts(self):
        login = self.client.login(username = 'admin', password = 'pass')
        response = self.client.get(reverse('post_draft_list'))
        self.assertEqual(response.status_code, 200)

        self.assertTrue(len(response.context['post_list'])==10)
