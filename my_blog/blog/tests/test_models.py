from django.test import TestCase
from blog.models import Post, Comment
from django.contrib.auth.models import User
from django.utils import timezone

# Create your tests here.

class PostModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")

    # TEST FIELD LABELS

    def test_author_label(self):
        post = Post.objects.get(id=1)
        field_label = post._meta.get_field('author').verbose_name
        self.assertEqual(field_label, 'author')

    def test_title_label(self):
        post = Post.objects.get(id=1)
        field_label = post._meta.get_field('title').verbose_name
        self.assertEqual(field_label, 'title')

    def test_text_label(self):
        post = Post.objects.get(id=1)
        field_label = post._meta.get_field('text').verbose_name
        self.assertEqual(field_label, 'text')

    def test_create_date_label(self):
        post = Post.objects.get(id=1)
        field_label = post._meta.get_field('create_date').verbose_name
        self.assertEqual(field_label, 'create date')

    def test_published_date_label(self):
        post = Post.objects.get(id=1)
        field_label = post._meta.get_field('published_date').verbose_name
        self.assertEqual(field_label, 'published date')

    # TEST MAX LENGTH

    def test_title_max_length(self):
        post = Post.objects.get(id=1)
        max_length = post._meta.get_field('title').max_length
        self.assertEqual(max_length, 200)

    # TEST CREATE DATE
    def test_default_create_date_value(self):
        post = Post.objects.get(id=1)
        date_default = post._meta.get_field('create_date').default
        self.assertEqual(date_default, timezone.now)

    def test_default_create_date_time_less_than_now(self):
        post = Post.objects.get(id=1)
        date = post.create_date
        self.assertLess(date, timezone.now())

    # TEST DEFAULT PUBLISHED DATE

    def test_default_published_date_blank_allowed(self):
        post = Post.objects.get(id=1)
        blank_default = post._meta.get_field('published_date').blank
        self.assertEqual(blank_default, True)

    def test_default_published_date_null(self):
        post = Post.objects.get(id=1)
        null_default = post._meta.get_field('published_date').null
        self.assertEqual(null_default, True)

    # TEST MODEL STRING REPRESENTATION

    def test_post_model_str_rep(self):
        post = Post.objects.get(id=1)
        str_rep = str(post)
        expected_str_rep = post.title
        self.assertEqual(str_rep, expected_str_rep)

    # TEST PUBLISH METHOD

    def test_published_date_not_null_after_publish(self):
        post = Post.objects.get(id=1)
        published_date = post.published_date

        self.assertIsNone(published_date)

        post.publish()

        published_date = post.published_date

        self.assertIsNotNone(published_date)

    # TEST get_absolute_url

    def test_get_absolute_url(self):
        post = Post.objects.get(id=1)
        self.assertEqual(post.get_absolute_url(), '/post/1')

    # TEST APPROVE COMMENTS FILTER

    def test_approve_comments(self):
        post = Post.objects.get(id=1)
        self.assertEqual(0, len(post.approve_comments()))

class CommentModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser('admin', 'admin@test.com', 'pass')
        Post.objects.create(author = User.objects.get(id=1), title = "Test Post", text = "Test text")
        Comment.objects.create(post = Post.objects.get(id=1), author = "Anon", text = "Comment text")

    # TEST FIELD LABELS
    def test_post_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('post').verbose_name
        self.assertEqual(field_label, 'post')

    def test_author_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('author').verbose_name
        self.assertEqual(field_label, 'author')

    def test_text_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('text').verbose_name
        self.assertEqual(field_label, 'text')

    def test_create_date_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('create_date').verbose_name
        self.assertEqual(field_label, 'create date')

    def test_approved_comment_label(self):
        comment = Comment.objects.get(id=1)
        field_label = comment._meta.get_field('approved_comment').verbose_name
        self.assertEqual(field_label, 'approved comment')

    # TEST MAX LENGTH

    def test_author_max_length(self):
        comment = Comment.objects.get(id=1)
        max_length = comment._meta.get_field('author').max_length
        self.assertEqual(max_length, 200)

    # TEST CREATE DATE

    def test_default_create_date_value(self):
        comment = Comment.objects.get(id=1)
        date_default = comment._meta.get_field('create_date').default
        self.assertEqual(date_default, timezone.now)

    def test_default_create_date_time_less_than_now(self):
        comment = Comment.objects.get(id=1)
        date = comment.create_date
        self.assertLess(date, timezone.now())

    # TEST DEFAULT APPROVED COMMENT

    def test_default_approved_comment(self):
        comment = Comment.objects.get(id=1)
        default_boolean = comment._meta.get_field('approved_comment').default
        self.assertEqual(default_boolean, False)

    # TEST MODEL STRING REPRESENTATION

    def test_comment_model_str_rep(self):
        comment = Comment.objects.get(id=1)
        str_rep = str(comment)
        expected_str_rep = comment.text
        self.assertEqual(str_rep, expected_str_rep)

    # TEST get_absolute_url

    def test_get_absolute_url(self):
        comment = Comment.objects.get(id=1)
        self.assertEqual(comment.get_absolute_url(), '/')

    # TEST APPROVE METHOD

    def test_approve_method(self):
        comment = Comment.objects.get(id=1)
        approved_comment = comment.approved_comment

        self.assertEqual(approved_comment, False)

        comment.approve()
        approved_comment = comment.approved_comment

        self.assertEqual(approved_comment, True)
