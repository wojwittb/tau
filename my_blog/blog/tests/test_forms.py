from django.test import TestCase
from django.test import Client
from blog.models import Post, Comment
from blog.forms import CommentForm, PostForm
from django.contrib.auth.models import User

# Create your tests here.

class PostFormTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser(username = 'admin', email = 'admin@test.com', password = 'pass')

    def test_post_form_valid(self):
        form = PostForm(data = {'author':1,'title':'Test Post','text':'Test text'})
        self.assertTrue(form.is_valid())

    def test_post_form_invalid_author(self):
        form = PostForm(data = {'author':2,'title':'smth','text':'abcde'})
        self.assertFalse(form.is_valid())

    def test_post_form_invalid_title_max_len(self):
        form = PostForm(data = {'author':1,'title':'smthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmthsmsmthsmthsmthsmththsmthsmth','text':'abcde'})
        self.assertFalse(form.is_valid())

class CommentFormTest(TestCase):

    def test_comment_form_valid(self):
        form = CommentForm(data = {'author':'Anon', 'text':'Test text'})
        self.assertTrue(form.is_valid())

    def test_comment_form_valid_author_max_len(self):
        form = CommentForm(data = {'author':'Anonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnonnon', 'text':'Test text'})
        self.assertFalse(form.is_valid())
