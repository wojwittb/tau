from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Chrome('/home/wostu/Documents/my_blog/chromedriver')

driver.get('http://127.0.0.1:8000/')

time.sleep(2)

login = driver.find_element_by_id('login').click()

time.sleep(2)

username = driver.find_element_by_id('id_username')
username.send_keys('test')
password = driver.find_element_by_id('id_password')
password.send_keys('pass1234')

time.sleep(2)

password.submit()

time.sleep(2)

driver.find_element_by_id('new').click()

time.sleep(2)

author = driver.find_element_by_id('id_author')
options = author.find_elements_by_tag_name('option')

options[1].click()

title = driver.find_element_by_id('id_title')
title.send_keys("Some test title")

time.sleep(2)

text = driver.find_element_by_id('id_text')
text.send_keys("Selenium is so much fun")
text.submit()

time.sleep(2)

driver.find_element_by_id('drafts')

time.sleep(2)

driver.find_element_by_id('publish').click()

time.sleep(2)

driver.quit()
