from locust import HttpLocust, TaskSet, task


# http://127.0.0.1:8089/
# locust --host=http://127.0.0.1:8000/



class UserBehaviour(TaskSet):
    def on_start(self):
        self.login()

    def login(self):
        response = self.client.get('/accounts/login/')
        csrftoken = response.cookies['csrftoken']

        self.client.post('/accounts/login/', {'username':'test', 'password':'pass1234'}, headers = {'X-CSRFToken':csrftoken})

    @task(1)
    def index(self):
        self.client.get('/')

    @task(2)
    def about(self):
        self.client.get('/about/')

    @task(3)
    def post_draft(self):
        response = self.client.get('/post/5/comment/')
        csrftoken = response.cookies['csrftoken']

        self.client.post('/post/5/comment/', {'author':'test_swarm', 'text':'Ni hao!'}, headers = {'X-CSRFToken':csrftoken})

class WebsiteUser(HttpLocust):
    task_set = UserBehaviour
