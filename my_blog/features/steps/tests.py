from behave import given, when, then
from blog.models import Post
from django.contrib.auth.models import User
from django.utils import timezone

# First test
@given(u'this step exists')
def step_exists(context):
    pass

@when(u'I run "python manage.py behave"')
def run_command(context):
    pass

@then(u'I should see the behave tests run')
def is_running(context):
    pass

# Second test
@when(u'I use django test client to visit "{url}"')
def use_django_client(context, url):
    context.response = context.test.client.get(url)

@then(u'it should return a successful response')
def it_should_be_successful(context):
    assert context.response.status_code == 200

# Third test
@when(u'I go to "{login_url}" and type in "{username}" and "{password}"')
def enter_info(context, login_url, username, password):
    usr = User.objects.create_user(username=username, password=password)
    
    context.response = context.test.client.get(login_url)
    csrftoken = context.response.cookies['csrftoken']
    context.test.client.post(login_url, {'username':username, 'password':password}, headers = {'X-CSRFToken':csrftoken})

@then(u'I should get successful response')
def check_view(context):
    print(context.response.status_code)
    assert context.response.status_code == 200

# Fourth test
@when(u'I create a post')
def create_post(context):
    usr = User.objects.create_user('Poster')
    Post.objects.create(author = usr, title = "BDD is fun", text = "behave-django is great", published_date = timezone.now())

@then(u'I should have "{num}" posts')
def count_posts(context, num):
    assert int(num) == Post.objects.count()
