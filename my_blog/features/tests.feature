Feature: Running tests

    Scenario: First test
        Given this step exists
        When I run "python manage.py behave"
        Then I should see the behave tests run

    Scenario: Django test client
       When I use django test client to visit "/about/"
       Then it should return a successful response

    Scenario: Login
       When I go to "/accounts/login/" and type in "test" and "pass1234"
       Then I should get successful response

    Scenario: Create Post
       When I create a post
       Then I should have "1" posts
