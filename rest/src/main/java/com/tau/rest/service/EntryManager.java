package com.tau.rest.service;

// w oparciu o przyklad J Neumanna, przerobiony przez T Puzniakowskiego

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.tau.rest.domain.Entry;

public interface EntryManager {
	public Connection getConnection();
	public int deleteEntry(Entry entry) throws SQLException;
	public int updateEntry(Entry entry) throws SQLException;
	public void clearEntries() throws SQLException;
	public int addEntry(Entry entry);
	public Entry getEntry(Entry entry);
	public List<Entry> getAllEntries();

}
