package com.tau.rest.domain;

import org.springframework.http.HttpStatus;

public class EntryDto {

	private Entry entry;
	private HttpStatus status;
	
	public Entry getEntry() {
		return entry;
	}
	public void setEntry(Entry entry) {
		this.entry = entry;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public EntryDto(){
		
	}
	
	public EntryDto(Entry entry, HttpStatus status) {
		super();
		this.entry = entry;
		this.status = status;
	}
	
	
	
	
}
