package com.tau.rest.domain;

public class Entry {
	
	private long id;
	private String name;
	private String text;
	
	public Entry() {
	}
	
	public Entry(String name, String text) {
		super();
		this.name = name;
		this.text = text;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}
