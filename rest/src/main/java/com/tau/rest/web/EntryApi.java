package com.tau.rest.web;

import com.tau.rest.domain.Entry;
import com.tau.rest.service.EntryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.tau.rest.domain.EntryDto;
import com.tau.rest.domain.ServerResponse;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;
//import java.util.concurrent.atomic.AtomicLong;

/**
 * Simple web api demo -- it only shows how to get some sample data
 *
 * tryout: ()
 *
 * Created by tp on 24.04.17.
 */
@RestController
public class EntryApi {
    //private final AtomicLong counter = new AtomicLong();

    @Autowired
    EntryManager entryManager;


    @RequestMapping("/")
    public String index() {
        return "Check check.";
    }

    @RequestMapping(
            value = "/entry/{entryName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public EntryDto getEntry(@PathVariable String entryName) {
    	Entry entry;
    	HttpStatus status;
    	
        try{
        	entry = this.entryManager.getEntry(new Entry(entryName,""));
        	status = HttpStatus.OK;
        	
        }catch(NoSuchElementException e){
        	entry = null;
        	status = HttpStatus.NOT_FOUND;
        }
        
    	return new EntryDto(entry,status);
    	
    }  
    
    @RequestMapping(
            value = "/entries",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public List<Entry> getAllEntries() {
    	
    	return entryManager.getAllEntries();
    }
    
    
    @RequestMapping(
            value = "/entry/{entryName}/delete",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ServerResponse deleteEntry(@PathVariable String entryName) {
    	
    	try {
			if(entryManager.deleteEntry(new Entry(entryName, "")) == 1) return new ServerResponse(HttpStatus.ACCEPTED, "Entry successfuly deleted");
			else return new ServerResponse(HttpStatus.NOT_FOUND, "No entry found to delete");
		} catch (SQLException e) {
			return new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong!");
		}
    	
    }
    
    
    @RequestMapping(
            value = "/addEntry",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ServerResponse addEntry(@RequestBody Entry entry) {
    	
    	if(entryManager.addEntry(entry) == 1) return new ServerResponse(HttpStatus.ACCEPTED, "Entry successfuly added");
		else return new ServerResponse(HttpStatus.NOT_ACCEPTABLE, "Cannot add that entry");
    	
    }
    
    @RequestMapping(
            value = "/updateEntry",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ServerResponse updateEntry(@RequestBody Entry entry) {
    	
    	try {
			if(entryManager.updateEntry(entry)==1)return new ServerResponse(HttpStatus.ACCEPTED, "Entry successfuly updated");
			else return new ServerResponse(HttpStatus.NOT_FOUND, "No entry found to update");
		} catch (SQLException e) {
			return new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong!");
		}
    }

}
