package com.tau.rest.tests;
// przyklad na podstawie przykladow J. Neumanna
import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

import com.tau.rest.domain.Entry;
import com.tau.rest.service.EntryManagerImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EntryManagerTest {

	EntryManagerImpl entryManager = new EntryManagerImpl();
	
	private final static String NAME_1 = "STEFAN";
	private final static String NAME_2 = "ANON";
	private final static String NAME_3 = "JAN";


	private final static String TEXT_1 = "abcde";
	private final static String TEXT_2 = "fghij";

	
	public EntryManagerTest() throws SQLException {
	}

	@Before
	@After
    public void cleanup() throws SQLException {
        entryManager.clearEntries();
    }

	@Test
	public void connectionTest() {
	    assertNotNull(entryManager.getConnection());
	}
	
	@Test
	public void addEntryTest() throws SQLException{
		Entry entry = new Entry(NAME_1, TEXT_1);
		
		assertEquals(1,entryManager.addEntry(entry));
		
		List<Entry> entries = entryManager.getAllEntries();
		Entry entryRetrieved = entries.get(0);
		
		assertEquals(NAME_1, entryRetrieved.getName());
		assertEquals(TEXT_1, entryRetrieved.getText());
	}
	
	@Test
	public void deleteEntryTest() throws SQLException{
		Entry entry = new Entry(NAME_1,TEXT_1);
		Entry entry2 = new Entry(NAME_2,TEXT_2);
		
		
		assertEquals(1,entryManager.addEntry(entry));
		assertEquals(1,entryManager.addEntry(entry2));

		assertEquals(1,entryManager.deleteEntry(entry));
		
		assertEquals(1,entryManager.getAllEntries().size());
	}
	
	@Test
	public void getEntryTest() throws SQLException{
		Entry entry = new Entry(NAME_1, TEXT_1);
		
		assertEquals(1,entryManager.addEntry(entry));
		
		Entry entryRetrieved = entryManager.getEntry(entry);
		
		assertEquals(NAME_1, entryRetrieved.getName());
		assertEquals(TEXT_1, entryRetrieved.getText());
	}
	
	@Test
	public void getAllEntriesTest() throws SQLException{
		Entry entry = new Entry(NAME_1, TEXT_1);
		Entry entry2 = new Entry(NAME_2, TEXT_2);
		
		assertEquals(1,entryManager.addEntry(entry));
		assertEquals(1,entryManager.addEntry(entry2));
		
		List<Entry> entries = entryManager.getAllEntries();
		
		assertEquals(2, entries.size());
		assertEquals(NAME_1, entries.get(0).getName());
		assertEquals(NAME_2, entries.get(1).getName());
	}
	
	@Test
	public void updateEntryTest() throws SQLException{
		Entry entry = new Entry(NAME_1, TEXT_1);
		Entry entry2 = new Entry(NAME_3, TEXT_2);

		assertEquals(1,entryManager.addEntry(entry));
		assertEquals(1,entryManager.addEntry(entry2));
		
		Entry entryupdate = new Entry(NAME_2, TEXT_1);
		assertEquals(0,entryManager.updateEntry(entryupdate));
		
		Entry entryRetrieved = entryManager.getEntry(entry);
		Entry entryRetrieved2 = entryManager.getEntry(entry2);
		
		assertEquals(NAME_1, entryRetrieved.getName());
		
	}

}
